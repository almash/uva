import java.util.Scanner;
import java.io.IOException;

public class U100 {
	private static int MAX = 1000000;
	public static void main(String args[]) throws IOException {
		Scanner sc = new Scanner(System.in);
		while(sc.hasNextInt()){
			int i = sc.nextInt();
	        int j = sc.nextInt();			
			
			int length=1, ti, tj;
			int maxlength = 0;
			ti = Math.min(i, j);
			tj = Math.max(i, j);
			if(ti > 0 && ti < MAX && tj > 0 && tj < MAX){
				for (int k = ti; k <= tj; k++){
				    length = f3n1(k);
				    maxlength = Math.max(maxlength, length);
				}
				System.out.println(i+" "+j+" "+maxlength);
			}
		}
	}
	
	public static int f3n1(int n) throws IOException {
		int length = 1;
		
		while(n != 1){
            if(n % 2 == 0){
            	n=n/2;
            	length++;
            }else{                
                n = 3*n +1;
                n=n/2;
                length +=2;
            }
            
        }
		
		
		return length; 
	}
}
