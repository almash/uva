import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;
import java.util.StringTokenizer;

/*
 * @author naf4me@gmail.com
 * UVA 101
 */
public class U101 {
    public static int NumBlocks;
    public static Stack<Integer> Nblocks[];
    public static int Posn[];
    public static String Sline; 
    public static int A, B;
    
    @SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException{
    	
    	// Take stream input from System
        BufferedReader Input = new BufferedReader(new InputStreamReader(System.in));
        
        NumBlocks = Integer.parseInt(Input.readLine());
        Nblocks = new Stack[NumBlocks];
        Posn = new int[NumBlocks];
        for(int i = 0; i < NumBlocks; i++) {
            Nblocks[i] = new Stack<Integer>();
            Nblocks[i].push(i);
            Posn[i] = i;
        }
        
        Sline = "";
        while(!(Sline = Input.readLine()).equals("quit")) {
            StringTokenizer token = new StringTokenizer(Sline);
            String First = token.nextToken();
            A = Integer.parseInt(token.nextToken());
            String Second = token.nextToken();
            B = Integer.parseInt(token.nextToken());
            
            if(A == B || Posn[A] == Posn[B]) continue;
            if(First.equals("move")) {
                if(Second.equals("onto")) {
                    MoveOnto(A, B);
                } else if(Second.equals("over")) {
                    MoveOver(A, B);
                }
            } else if(First.equals("pile")) {
                if(Second.equals("onto")) {
                    PileOnto(A, B);
                } else if(Second.equals("over")) {
                    PileOver(A, B);
                }
            }
        }
        for(int i = 0; i < Nblocks.length; i++) { 
        	System.out.println(Solve(i));
        }
    }
    public static void MoveOnto(int First, int Second) {
       ClearAbove(Second);
       MoveOver(First, Second);
    }
    public static void MoveOver(int First, int Second) {   
       ClearAbove(First);
       Nblocks[Posn[Second]].push(Nblocks[Posn[First]].pop());
       Posn[First] = Posn[Second];
    }
    public static void PileOnto(int First, int Second) {
       ClearAbove(Second);
       PileOver(First, Second);
    }
    public static void PileOver(int First, int Second) {
       Stack<Integer> Pile = new Stack<Integer>();
       while(Nblocks[Posn[First]].peek() != First) {
           Pile.push(Nblocks[Posn[First]].pop());
       }
       Pile.push(Nblocks[Posn[First]].pop());
       while(!Pile.isEmpty()) {
          int Tmp = Pile.pop();
          Nblocks[Posn[Second]].push(Tmp);
          Posn[Tmp] = Posn[Second];
       }
    }
    public static void ClearAbove(int Block) {
        while(Nblocks[Posn[Block]].peek() != Block) {
           Intial(Nblocks[Posn[Block]].pop());
        }
    }
    public static void Intial(int Block) {   
        while(!Nblocks[Block].isEmpty()) {
           Intial(Nblocks[Block].pop());
        }
        Nblocks[Block].push(Block);
        Posn[Block] = Block;
    }
    public static String Solve(int Index) {
        String Result = "";
        while(!Nblocks[Index].isEmpty()) Result = " " + Nblocks[Index].pop() + Result;
        Result = Index + ":" + Result;
        return Result;
    }

}